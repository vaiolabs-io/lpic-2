# LPIC-2

## Linux Professional Institute Certification 2
- 201-450
    - [Capacity Planning](./201-450/01_capacity_planning/README.md)
    - [Linux Kernel](./201-450/02_linux_kernel/README.md)
    - [System Startup](./201-450/03_system_startup/README.md)
    - [Filesystems & Devices](./201-450/04_filesystem_and_devices/README.md)
    - [Advanced Storage Administration](./201-450/05_adv_storage_administation/README.md)
    - [Network Configuration](./201-450/06_net_configuration/README.md)

- 202-450
    - [Domain Name Service](./202-450/01_domain_name_service/README.md)
    - [Web Services](./202-450/02_web_services/README.md)
    - [File Sharing](./202-450/03_file_sharing/README.md)
    - [Network Client Management](./202-450/04_net_client_mngmt/README.md)
    - [Email Services](./202-450/05_email_services/README.md)
    - [System Security](./202-450/06_system_security/README.md)