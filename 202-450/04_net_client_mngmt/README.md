
---
# Building DHCP server

- install dhcp
- configure minimum params:
    - ip range
- configure full dhcp with params
    - interface
    - lease
    - route
    - subnet
    - ip range
    - dns address
    - domain search
- test configuration locally
- enable service
- check lease file
- open firewall to 67/udp port

---

# Configuring multiple subnets in dhcp

- configure dhcp server to 2 different interfaces 
- static dhcp mapping with mac address
- 

---

# Building authentication server with openldap

- What is openldap ?
- Install slapd ldap-utils
    - slapcat
    - /etc/ldap.conf => good luck with pain
    -  dpkg reconfigure slapd ==> should provide auto-menu to guide setup
    - php-ldap-admin => conf file
    - disable apache2 default site
    - firewall 389 ldap open

---

# Managing Objects in openldap

---

# Configuring openldap with tls

---

# Connecting a client to openldap

